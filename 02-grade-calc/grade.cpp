/*
 * Name: Rhett Gordon
 * Date: 1/31/2019
 * Section: 7
 * Assignment: 2
 * Due Date: 1/31/2019
 * About this project: Create a program that calculates the final grade for the course, following the syllabus grading
 *                     scale as well the given specs for the assignment.
 * Assumptions: Assumes user will not input non-numeric data when prompted.
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>
using namespace std;

int main() {
    //init variables
    //this is honestly kind of a mess
    int i;                      //loop control variable
    char input;                 //used for Y or N input in one part of the program
    bool flag = false;          //validation control variable
    bool errorFlag = false;     //I need this for an error exit, because I can only have one return as per the specs
    int homeworkGrades[7] = { };    int hwSum = 0;
    int recitationGrades[6] = { };  int rcSum = 0;
    int testGrades[3] = { };        //Tests are handled different, so sum won't be stored here.
    double hwAvg, tstAvg, courseAvg;
    int extraCredit = 0;

    //Output intro message
    cout << "Welcome to the Grade Calculator!\n\n";

    //fetch homework grades
    cout << "HOMEWORK GRADES -->\n";
    for (i = 0; i < 7; i++) {
        do {
            cout << "Homework " << (i + 1) << ": ";
            cin >> homeworkGrades[i];
            if (homeworkGrades[i] < 0) {
                cout << "Bad input.\n";
                flag = false;
            } else {
                flag = true;
            }
        } while (!flag);
        hwSum += homeworkGrades[i];
    }

    //fetch recitation grades
    cout << "\nRECITATION GRADES -->\n";
    for (i = 0; i < 6; i++) {
        do {
            cout << "Recitation " << (i + 1) << ": ";
            cin >> recitationGrades[i];
            if (recitationGrades[i] == 0 || recitationGrades[i] == 10 || recitationGrades[i] == 20) {
                flag = true;
            } else {
                cout << "Bad input. Must be either 0, 10, or 20.\n";
                flag = false;
            }
        } while (!flag);
        rcSum += recitationGrades[i];
    }

    //fetch test grades
    cout << "\nTEST GRADES -->\n";
    for (i = 0; i < 3; i++) {
        do {
            switch (i) {
                case 0:
                    cout << "Midterm I: ";
                    cin >> testGrades[0];
                    break;
                case 1:
                    cout << "Midterm II: ";
                    cin >> testGrades[1];
                    break;
                case 2:
                    cout << "Final: ";
                    cin >> testGrades[2];
                    break;
                default: //necessary if, somehow or another, i goes beyond out hard-coded value of 3
                    cout << "Error: Input will go out of bounds. Not accepting inputs.";
            }
            if (testGrades[i] < 0) {
                cout << "Bad input.\n";
                flag = false;
            } else {
                flag = true;
            }
        } while (!flag);
    }

    //Check for extra credit to add in
    cout << "\nIs there any extra credit to add in? (Y or N) --> ";
    do {
        cin >> input;
        if (input == 'Y') {
            flag = true;
            cout << "How many extra credit points should be added? --> ";
            cin >> extraCredit;
        } else if (input == 'N') {
            //Valid input, does nothing
            flag = true;
        } else {
            cout << "Invalid option, exiting program.";
            flag = true;
            errorFlag = true;
        }
    } while (!flag);

    //Calculation phase, meant to exit if bad input is made at this point
    if (!errorFlag) {
        //Setup
        cout.setf(ios::fixed);
        cout.precision(2);

        //Perform calculation for course grades
        hwAvg = (double(hwSum + rcSum + extraCredit) / 820) * 100;
        cout << "\nHomework Average: " << hwAvg << "%\n";

        tstAvg = (((testGrades[0] * 17.5) + (testGrades[1] * 17.5) + (testGrades[2] * 25)) / 60);
        cout << "Test Average: " << tstAvg << "%";
        if (tstAvg <= 69) {
            cout << " (!!Test average requirement not met, final course grade may differ!!)\n";
        } else {
            //No output, newline needed though
            cout << endl;
        }

        courseAvg = (testGrades[0] * .175) + (testGrades[1] * .175) + (testGrades[2] * .25) + (hwAvg * .4);
        cout << "Course Average: " << courseAvg << "%\n";

        //Output final grade for the course
        cout << "Course Grade: ";
        if (courseAvg >= 92) {
            cout << "A";
        } else if (courseAvg >= 90) {
            cout << "A-";
        } else if (courseAvg >= 88) {
            cout << "B+";
        } else if (courseAvg >= 82) {
            cout << "B";
        } else if (courseAvg >= 80) {
            cout << "B-";
        } else if (courseAvg >= 78) {
            cout << "C+";
        } else if (courseAvg >= 72) {
            cout << "C";
        } else if (courseAvg >= 69) {
            cout << "C-";
        } else if (courseAvg >= 62) {
            cout << "D";
        } else if (courseAvg >= 60) {
            cout << "D-";
        } else {
            cout << "F";
        }
    }

    return 0;
}
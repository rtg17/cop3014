# COP3014

This repository consists of project files for assignments I had completed during the Spring 2019 term at Florida State University.

### Note
I created this repo to share the work I had done for this course. I don't intend for others to find this with the intent of not doing the homework. That's called plagarism, and it **will** get you in hot water.
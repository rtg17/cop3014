/* Name:                Rhett Gordon
 * Date:                March 15th, 2019
 * Section:             6
 * Assignment:          5
 * Due Date:            March 15th, 2019
 * About this project:  Manipulates arrays in a variety of ways, with three required functions
 * Assumptions:         Assuming int input isn't done improperly (should probably read up on how to not have that happen
 *                      at some point).
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>
#include <cstdlib>
#include <ctime>
#define MAX 40
#define MIN 5

//Required functions
void ReverseArray(int arr[], int length);                       //REQUIRED FUNCTION
void ShiftRight(int arr[], int length, int distance);           //REQUIRED FUNCTION
void DeleteArrayElement(int arr[], int length, int position);   //REQUIRED FUNCTION

//Some other functions
void Menu();
void PrintArray(const int arr[], int length);
void PopulateArray(int arr[], int length);
void ChangeArrayElement(int arr[], int position, int value);
double ReturnAvg(const int arr[], int length);
int ReturnMaxVal(const int arr[], int length);
void GrowArray(int arr[], int& length);
void ShrinkArray(int& length);


int main() {
    int list[MAX], currentSize = MIN;
    bool arrayPopulated = false;
    char inputChar;
    int inputInt = 0;
    int hold;

    //Welcome message
    std::cout << "Welcome to the array manipulator!\n";

    //Begin by having the user work on populating the array with random numbers
    std::cout << "How big would you like to make your array?\nEnter a value 5 --> 40: ";
    while (!arrayPopulated) {
        std::cin >> inputInt;
        if (inputInt < 5 || inputInt > 40) {
            std::cout << "Try again: ";
        } else {
            currentSize = inputInt;
            PopulateArray(list, currentSize);
            arrayPopulated = true;
        }
    }

    do {
        Menu();
        std::cin >> inputChar;
        switch(toupper(inputChar)) {
            case 'F':
                PopulateArray(list, currentSize);
                break;
            case 'R':
                ReverseArray(list, currentSize);
                break;
            case 'X':
                std::cout << "Shift right by: ";
                std::cin >> inputInt;
                ShiftRight(list, currentSize, inputInt);
                break;
            case 'C':
                std::cout << "What array index would you like to update? > ";
                std::cin >> inputInt;
                if (inputInt > currentSize - 1 || inputInt < 0) {
                    std::cout << "Cannot change arr[" << inputInt << "]. Out of bounds.";
                } else {
                    hold = inputInt;
                    std::cout << "Change arr[" << hold << "] to what value? > ";
                    std::cin >> inputInt;
                    ChangeArrayElement(list, hold, inputInt);
                }
                break;
            case 'D':
                std::cout << "Delete element at which index? > ";
                std::cin >> inputInt;
                if (inputInt > currentSize) {
                    std::cout << "Cannot delete element at arr[" << inputInt << "], arr size is: " << currentSize << std::endl;
                } else {
                    DeleteArrayElement(list, currentSize, inputInt);
                }
                break;
            case 'A':
                std::cout << "Average: " << ReturnAvg(list, currentSize) << std::endl;
                break;
            case 'M':
                std::cout << "Max Value: " << ReturnMaxVal(list, currentSize) << std::endl;
                break;
            case 'G':
                if (currentSize >= 40) {
                    std::cout << "Cannot Grow Array. Current size: " << currentSize << " Maximum Size: " << MAX << std::endl;
                } else {
                    GrowArray(list, currentSize);
                }
                break;
            case 'S':
                if (currentSize == 5) {
                    std::cout << "Cannot Shrink Array. Current size: " << currentSize << " Minimum Size: " << MIN << std::endl;
                } else {
                    ShrinkArray(currentSize);
                }
                break;
            case 'P':
                PrintArray(list, currentSize);
                break;
            case 'Q':
                std::cout << "Goodbye!";
                break;
            default:
                std::cout << toupper(inputChar) << " is not a valid selection.\n\n";
        }
    } while (toupper(inputChar) != 'Q');

    return 0;
}

void Menu() {
    std::cout << std::endl;
    std::cout << "MENU:  (or SELECT Q to QUIT)\n";
    std::cout << "F - Fill Array: RANDOM values 1 - 100\n";
    std::cout << "R - Reverse Array Contents\n";
    std::cout << "X - Shift Right\n";
    std::cout << "C - Change Array Element\n";
    std::cout << "D - Delete Array Element\n";
    std::cout << "A - Print Average\n";
    std::cout << "M - Print Max Value\n";
    std::cout << "G - Grow Array Size by 5\n";
    std::cout << "S - Shrink Array Size by 5\n";
    std::cout << "P - Print Array\n";
    std::cout << "---------------------------\n";
    std::cout << "Selection > ";
}

void PrintArray(const int arr[], int length) {
    int i;

    std::cout << std::endl;
    std::cout << "The array:\n{ ";
    for (i = 0; i < length; i++) {
        std::cout << arr[i];
        if (i < length - 1) {
            std::cout << ", ";
        }
    }
    std::cout << " }\n";
}

void ReverseArray(int arr[], int length) {
    int hold, i;
    for (i = 0; i < length / 2; i++) {
        hold = arr[i];
        arr[i] = arr[(length-1)-i];
        arr[(length-1)-i] = hold;
    }
}

void ShiftRight(int arr[], int length, int distance) {
    int hold; //storage variale
    int i, j; //loop control variables
    for (i = 0; i < distance; i++) {
        hold = arr[length - 1]; //Hold on to the last element, as we're going to lose it and will need it later
        for (j = length - 2; j >= 0; j--) {
            if (j == 0) { //If we're at the logical end of the loop
                arr[j] = hold;
            } else { //Otherwise
                arr[j+1] = arr[j];
            }
        }
    }
}

void DeleteArrayElement(int arr[], int length, int position) {
    int i;
    for (i = 0; i < length - position; i++) {
        if (i + position == length - 1) {
            arr[i+position] = 0;
        } else {
            arr[i+position] = arr[(i+position)+1];
        }
    }
}

void PopulateArray(int arr[], int length) {
    int i;
    srand(static_cast<unsigned int>(time(nullptr)));
    for (i = 0; i < length; i++) {
        arr[i] = rand() % 100 + 1;
    }
    PrintArray(arr, length);
}

void ChangeArrayElement(int arr[], int position, int value) {
    arr[position] = value;
}

double ReturnAvg(const int arr[], int length) {
    double sum = 0;
    int i;
    for (i = 0; i < length; i++) {
        sum = sum + arr[i];
    }
    return sum / length;
}

int ReturnMaxVal(const int arr[], int length) {
    int i, result = 0;
    for (i = 0; i < length; i++) {
        if (result < arr[i]) {
            result = arr[i];
        }
    }
    return result;
}

void GrowArray(int arr[], int& length) {
    int i;
    for (i = length; i < length + 5; i++) {
        arr[i] = 0;
    }
    length = length + 5;
}

void ShrinkArray(int& length) {
    length = length - 5;
}
/* Name:                Rhett Gordon
 * Date:                new Date();
 * Section:             6
 * Assignment:          6
 * Due Date:            April 2nd, 2019
 * About this project:  Utilizes strings for random sentence generation
 * Assumptions:         Assumes that user will enter five strings, separated by spaces
 *                      Assumes that all strings entered will be valid strings that meet the criteria
 *
 * All work below was performed by Rhett Gordon
 */

//Needed libraries
#include <iostream>
#include <cctype>
#include <cstring>
#include <cstdlib>
#include <ctime>

//Macro definitions
#define MAX_CHARS 20
#define STRINGS_ALLOTTED 5

//Prototypes
void generatePhrase(const char article[5][5], const char noun[STRINGS_ALLOTTED][MAX_CHARS], const char verb[STRINGS_ALLOTTED][MAX_CHARS], const char preposition[5][6], int count);

int main() {
    char noun[STRINGS_ALLOTTED][MAX_CHARS];
    char verb[STRINGS_ALLOTTED][MAX_CHARS];
    char article[5][5] = {
            "the",
            "a",
            "one",
            "some",
            "any"
    };
    char preposition[5][6] = {
            "to",
            "from",
            "over",
            "under",
            "on"
    };
    int i, j;

    //Get noun from the user
    std::cout << "Enter " << STRINGS_ALLOTTED << " noun: ";
    std::cin >> noun[0] >> noun[1] >> noun[2] >> noun[3] >> noun[4];

    //Get verb from the user
    std::cout << "Enter " << STRINGS_ALLOTTED << " past tense verb: ";
    std::cin >> verb[0] >> verb[1] >> verb[2] >> verb[3] >> verb[4];

    //make the values of both arrays lower case
    for (i = 0; i < STRINGS_ALLOTTED; i++) {
        for (j = 0; j < MAX_CHARS; j++) {
            if (isupper(noun[i][j])) {
                noun[i][j] = tolower(noun[i][j]);
            }
            if (isupper(verb[i][j])) {
                verb[i][j] = tolower(verb[i][j]);
            }
        }
    }


    generatePhrase(article, noun, verb, preposition, 20);

    return 0;
}

void generatePhrase(const char article[5][5], const char noun[STRINGS_ALLOTTED][MAX_CHARS], const char verb[STRINGS_ALLOTTED][MAX_CHARS], const char preposition[5][6], int count) {
    srand(time(nullptr));
    char output[MAX_CHARS * STRINGS_ALLOTTED + STRINGS_ALLOTTED]; //PEMDAS should make this become a char[105], enough for the four spaces and single period.
    int i;

    for (i = 0; i < count; i++) {
        //Begin structuring our random sentence
        strcpy(output, article[rand() % 5]);
        strcat(output, " ");
        strcat(output, noun[rand() % STRINGS_ALLOTTED]);
        strcat(output, " ");
        strcat(output, verb[rand() % STRINGS_ALLOTTED]);
        strcat(output, " ");
        strcat(output, preposition[rand() % 5]);
        strcat(output, " ");
        strcat(output, article[rand() % 5]);
        strcat(output, " ");
        strcat(output, noun[rand() % STRINGS_ALLOTTED]);
        strcat(output, ".");

        //Make the first character capitalized
        if (islower(output[0])) {
            output[0] = toupper(output[0]);
        }

        //Output our beautiful randomized sentence
        std::cout << output << std::endl;
    }
}
/*
 * Name: Rhett Gordon
 * Date: 2/7/2019
 * Section: 7
 * Assignment: 3
 * Due Date: 2/15/2019
 * About this project: Create a menu program that snags inputs from a user and spits out an average, utilizing both a
 *                     value-returning and non-value-returning function
 * Assumptions: Assumes user will not input non-numeric data when prompted.
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>
using namespace std;

//Prototyping
void menu();
double OrderTotal(int, int, int, int);

int main() {
    int input, menuSelection;       //menuSelection is used to store which menu item the user selected, so that input
                                    //could be reused.
    int order[4] = { 0, 0, 0, 0 };  //Some rescinded part of my brain said "make this an array", but it probably could
                                    //have been fine if it were just separate variables all together.
    bool flag = false;              //Loop control variable

    while (!flag) {
        menu();
        cout << "\nWhat would you like? (Enter 0 to finish order!): ";
        cin >> input;
        if (input == 0) {   //Start by checking if the user wants to quit
            flag = true;
        } else if (input >= 1 && input <= 4) {  //Now check to see if the user made a legal input
            menuSelection = input - 1;
            do {    //Separate loop, to ensure user isn't entering a negative amount of something
                cout << "How many: ";
                cin >> input;
                if (input < 0) {
                    cout << "You can't order a negative amount of food!\n";
                }
            } while (input < 0); //0 is a valid value, according to the specs
            order[menuSelection] += input;
            cout << endl; //Linebreak
        } else { //Invalid input
            cout << "That isn't on the menu! Try again.\n\n";
        }
    }

    if (order[0] == 0 && order[1] == 0 && order[2] == 0 && order[3] == 0) {
        cout << "Guess you aren't hungry.\n";
    } else {
        cout.setf(ios::fixed);
        cout.precision(2);

        cout << "Your order:\n";
        if (order[0] > 0) {
            cout << order[0] << " krabby patties.\n";
        }
        if (order[1] > 0) {
            cout << order[1] << " barnicle fries.\n";
            //No need for plural here
        }
        if (order[2] > 0) {
            cout << order[2] << " kelp shakes.\n";
        }
        if (order[3] > 0) {
            cout << order[3] << " krusty krab pizzas.\n";
        }
        cout << "\n\nYour total is $" << OrderTotal(order[0], order[1], order[2], order[3]);
        cout << "\nEnjoy the food!";
    }

    return 0;
}

//Additional functions
void menu() {
    cout << "----------- Menu -----------\n";
    cout << "1. Krabby Patty ...... $3.50\n";
    cout << "2. Barnicle Fries .... $1.50\n";
    cout << "3. Kelp Shake ........ $1.00\n";
    cout << "4. Krusty Krab Pizza . $5.00\n";
}

double OrderTotal(int itemOne, int itemTwo, int itemThree, int itemFour) {
    //Nothing too complicated is going on here
    return ((itemOne * 3.5) + (itemTwo * 1.5) + itemThree + (itemFour * 5));
}
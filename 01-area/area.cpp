/* Name:                    Rhett Gordon
 * Date:                    January 22nd, 2019
 * Section:                 06
 * Assignment:              01
 * Due Date:                January 23rd, 2019
 * About this project:      Small first project that performs arithmetic operations.
 * Assumptions:             User will input numeric data and not characters, as that's not necessarily checked here.
 *                          User won't make incorrect inputs on numeric data with floating points (i.e. double periods)
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>
using namespace std;
#define PI 3.14159

int main() {
    //Initialize variables
    double circleRadius, squareLength, triangleLength, triangleHeight;

    //Output words, input vars
    cout << "Welcome to the COP3014 Area Calculator!\n";
    cout << "Please provide the follow (all sizes in meters):\n";
    //Get circle radius
    cout << "\tRadius of the Circle: ";
    cin >> circleRadius;
    //Get square length
    cout << "\tSide Length of Square: ";
    cin >> squareLength;
    //Get triangle base length
    cout << "\tBase Length of the Triangle: ";
    cin >> triangleLength;
    //Get triangle height
    cout << "\tHeight of the Triangle: ";
    cin >> triangleHeight;

    //Output results
    cout.setf(ios::fixed);      //Project demands decimal points be shown even when ending in zeros
    cout.precision(2);          //Needed for floating point precision, to two decimal places
    cout << "--------------------------------------------------\n\n";
    cout << "Area of the Circle: " << (circleRadius * 2) * PI << "m^2\n";
    cout << "Area of the Square: " << squareLength * squareLength << "m^2\n";
    cout << "Area of the Triangle: " << (triangleHeight * triangleLength) / 2 << "m^2\n";
    cout << "\nThanks for using the COP3014 Area Calculator!";

    return 0;
}
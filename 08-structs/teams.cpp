/* Name:                Rhett Gordon
 * Date:                April 26th, 2019 (I forgot I left a placeholder in last time)
 * Section:             7 (turns out I still haven't figured out my section up until now)
 * Assignment:          7
 * Due Date:            April 26th, 2019
 * About this project:  Imports data from a file and utilizes it in a dynamic array of a data structure
 * Assumptions:         Assumes that the given file will have the correct data and formatting we need for the program to function
 *
 * All work below was performed by Rhett Gordon
 * _(」∠ ､ﾝ､)_
 */
#include <iostream>
#include <fstream>

struct Team {
    char name[50];
    int wins;
    int losses;
    int playoffs;
};

void menu();
void showRecords(Team team[], int size);
void showWins(Team team[], int size);
void madePlayoffs(Team team[], int size);
void showDifferentials(Team team[], int size);
void sortDifferentials(Team team[], int size);

int main() {
    Team *teams;
    int size, i, input;
    char location[100] = "teams.txt";
    std::ifstream file;
    bool flag = false;

    //Get the file name/location, and pop it open
    while (!flag) {
        std::cout << "Please enter the name of the input file.\nFilename: ";
        std::cin >> location;
        file.open(location);
        if (!file) {
            std::cout << "That is not a valid file.  Try again!\n";
        } else {
            flag = true;
        }
    }

    //Populate the array based on the size given
    file >> size;
    teams = new Team[size];
    for (i = 0; i < size; i++) {
        file.ignore(1, '\n');                   //Ignore the starting newline that will appear consistently
        file.getline(teams[i].name, 50, ',');   //Get the team name, as far as 50 chars since that's what we alloted
        file >> teams[i].wins;                  //Slap the first int we find into the wins
        file.ignore(3, ',');                    //Ignore the next comma we see by a wide margin
        file >> teams[i].losses;                //Do the same as the last int
        file.ignore(3, ',');                    //Do the ignore thing again
        file >> teams[i].playoffs;              //We just gotta get the last int now, and then probably loop back to ignore the newline
    }

    //We don't really need the file now
    file.close();

    do {
        menu();
        std::cin >> input;
        std::cout << '\n';
        switch(input) {
            case 1:
                showRecords(teams, size);
                break;
            case 2:
                showWins(teams, size);
                break;
            case 3:
                madePlayoffs(teams, size);
                break;
            case 4:
                showDifferentials(teams, size);
                break;
            case 5:
                sortDifferentials(teams, size);
                break;
            case 0:
                std::cout << "Goodbye!";
                break;
            default:
                std::cout << "Invalid Choice.";
        }
        std::cout << '\n';
    } while (input != 0);

    //End-of-program routine
    delete [] teams;
    return 0;
}

void menu() {
    std::cout << "Select one of the following choices:\n";
    std::cout << "\t1 - View Win/Loss records\n";
    std::cout << "\t2 - Show teams with at least x wins\n";
    std::cout << "\t3 - List teams that made the playoffs\n";
    std::cout << "\t4 - List teams and their differential\n";
    std::cout << "\t5 - Sort teams by their Win Loss Differentials\n";
    std::cout << "\t0 - Exit the program\n";
    std::cout << "----------------------------------------------\n";
    std::cout << "Choice: ";
}

void showRecords(Team team[], int size) {
    int i;

    std::cout << "Team W-L:\n";
    for (i = 0; i < size; i++) {
        std::cout << team[i].name << " " << team[i].wins << "-" << team[i].losses << "\n";
    }
}

void showWins(Team team[], int size) {
    int i, input;

    std::cout << "Show teams with at least how many wins? --> ";
    std::cin >> input;

    for (i = 0; i < size; i++) {
        if (team[i].wins >= input) {
            std::cout << team[i].name << "\n";
        }
    }
}

void madePlayoffs(Team team[], int size) {
    int i;
    bool outputStarted = false;

    for (i = 0; i < size; i++) {
        if (team[i].playoffs == 1) {
            if (i < size && outputStarted) {
                std::cout << ", ";
            }
            std::cout << team[i].name;
            if (!outputStarted) {
                outputStarted = true;
            }
        }
    }
    std::cout << '\n';
}

void showDifferentials(Team team[], int size) {
    int i;

    for (i = 0; i < size; i++) {
        std::cout << team[i].name;
        if (team[i].wins > team[i].losses) {
            std::cout << " +"; //We want a + to show if the result is going to be positive. We're letting Jesus take the wheel for the -.
        } else {
            std::cout << " ";
        }
        std::cout << (team[i].wins - team[i].losses) << "\n";
    }
}

void sortDifferentials(Team team[], int size) {
    int i, j, temp;
    int diffs[size][2];

    //Populate the temp array
    for (i = 0; i < size; i++) {
        diffs[i][0] = (team[i].wins - team[i].losses);  //A differential that we will allow to be shown to the end-user (less math)
        diffs[i][1] = i;                                //Essentially should be the index of the team with that differential, needed for the output
    }

    //Sort array
    for (i = 0; i < size - 1; i++) {
        for (j = 0; j < size - i - 1; j++) {
            if (diffs[j][0] > diffs[j + 1][0]) {
                //Start wih the value of the differential
                temp = diffs[j][0];
                diffs[j][0] = diffs[j + 1][0];
                diffs[j + 1][0] = temp;

                //Then swap the associated team ID
                temp = diffs[j][1];
                diffs[j][1] = diffs[j + 1][1];
                diffs[j + 1][1] = temp;
            }
        }
    }

    //Perform output
    for (i = 0; i < size; i++) {
        std::cout << team[diffs[i][1]].name; //Using the second column (row? i forget how these are organized sometimes) to output the proper team name
        if (team[diffs[i][1]].wins > team[diffs[i][1]].losses) {
            std::cout << " +"; //Repeat of the previous function's output conditioning
        } else {
            std::cout << " ";
        }
        std::cout << diffs[i][0] << "\n";
    }
}
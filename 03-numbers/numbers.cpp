/*
 * Name: Rhett Gordon
 * Date: 2/7/2019
 * Section: 7
 * Assignment: 3
 * Due Date: 2/15/2019
 * About this project: Create a program which gets an infinite amount of numbers, and spits out information regarding
 *                     the inputs made from the user.
 * Assumptions: Assumes user will not input non-numeric data when prompted.
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>
using namespace std;

int main() {
    bool flag = false;
    int input, sum = 0, numOfPositives = 0, numOfNegatives = 0;

    while (!flag) {
        cout << "Enter a number (0 to stop): ";
        cin >> input;
        if (input == 0) {
            flag = true;
        } else {
            sum += input;
            if (input > 0) {
                numOfPositives++;
            } else {
                numOfNegatives++;
            }
        }
    }

    cout.setf(ios::fixed);
    cout.precision(2);

    cout << "\n# of positives: " << numOfPositives;
    cout << "\n# of negatives: " << numOfNegatives;
    cout << "\nSum = " << sum;
    cout << "\nAverage = " << double(sum) / (numOfPositives + numOfNegatives);  //We have to treat sum as a double so
                                                                                //that the result of the expression
                                                                                //doesn't end up lacking the needed
                                                                                //precision.

    return 0;
}
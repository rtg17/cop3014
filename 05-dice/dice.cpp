/* Name:                    Rhett Gordon
 * Date:                    Feburary 28th, 2019
 * Section:                 06
 * Assignment:              04
 * Due Date:                March 01st, 2019
 * About this project:      Project utilizing random number generation
 * Assumptions:             User will input numeric data and not characters, as that's not necessarily checked here.
 *                          User won't make incorrect inputs on numeric data with floating points (i.e. double periods)
 *
 * All work below was performed by Rhett Gordon
 */
#include <iostream>     //Standard IO library
#include <cstdlib>	    // for srand(), rand()
#include <ctime>        // for time()
using namespace std;

//Func prototyping
void startMsg();
int rollDice();

int main() {
    //Declare some variables
    int input = 0;
    int results[3] = { 0, 0, 0 };
    int i;

    //Set up the stuff
    cout.setf(ios::fixed);
    cout.precision(2);
    srand((unsigned int)time(nullptr));

    startMsg();
    do {
        cout << "\nHow many times should we roll the dice?\n";
        cin >> input;
        //Conditional used here, as we need to verify some more specific inputs
        if (input == 0) { //Terminate the program
            cout << "See ya!";
        } else if (input < 0) { //Negative number input, returning an error message
            cout << "Cannot roll dice a negative number of times.\n";
        } else { //Standard activity
            cout << "Rolling dice " << input << " times...";
            for (i = 0; i < input; i++) {
                //No need for a default in the following switch, as we only need one of three conditions to be met.
                switch (rollDice()) {
                    case 2:
                        results[0]++;
                        break;
                    case 7:
                        results[1]++;
                        break;
                    case 12:
                        results[2]++;
                }
            }
            //We should treat the values as a double, so to ensure the division works out without error.
            cout << "\n\nResults:\n";
            cout << "A roll of 2 appeared " << results[0] << " total times, " << (double(results[0]) / input) * 100 << "% of the time.\n";
            cout << "A roll of 7 appeared " << results[1] << " total times, " << (double(results[1]) / input) * 100 << "% of the time.\n";
            cout << "A roll of 12 appeared " << results[2] << " total times, " << (double(results[2]) / input) * 100 << "% of the time.\n";
            //We're going to reset the results array, since we're going to presume by this point that the user might want
            //to do this again. Also the stats are largely irrelevant.
            for (i = 0; i < 3; i++) {
                results[i] = 0;
            }
        }
    } while (input != 0);

    return 0; //End of program
}

void startMsg () {
    cout << "Welcome to the dice stats calculator!\n";
    cout << "Let us know how many times to roll the dice!\n";
    cout << "Enter 0 whenever you're ready to quit\n";
}

int rollDice() {
    int result = (rand() % 6 + 1) + (rand() % 6 + 1);

    //Debugging version of the previous line, meant to ensure that the dice rolls were working right
    //Uncomment the block if you need to ensure it's working right
    /*
    int result, a, b;
    a = rand() % 6 + 1;
    b = rand() % 6 + 1;
    result = a + b;
    cout << "[DEBUG] First roll: " << a << "\tSecond roll: " << b << "\n[DEBUG] Result: " << result << endl;
    */

    return result;
}
/*
 * This document should help document some basic stuff you should know for the COP3014 course, in case you're ever lost
 * on anything.
 */

#include <iostream>     //Input/output library
using namespace std;    //Using the standard library by default
                        //This piece helps reduce the amount of things we have to type when doing input and output
                        //Normally, you would have to type std::cout, for instance.

#define SIZE 3      //We're defining a word we're going to refer to as SIZE, which has a size of three.
                    //While a global constant would have probably sufficed, this would save up on memory and simply
                    //tell the compiler that it needs to replace all instances of "SIZE" with "3". Also, the professor
                    //will duck points for use of global variables, as to discourage you from messing up variable scope.

void printSumAvg(const double[]);    //Prototyping, a method of declaring a function.
                                    //While I could also write the printSumAvg function up here, prototyping this allows me
                                    //to say "this is a function that I will use" to the compiler, which will see the
                                    //function below main. This is mostly good for if you want to iron out main before
                                    //writing any sort of outside function.
                                    //Also note that I declared the variable as a constant. I will iterate on this below.

int main() {
    double nums[SIZE] = {};             //Local variables. Their scope is going to be within main, and cannot be accessed
    int i;                              //outside of it. The first of which is an array, which is assigned with a size
                                        //out of the box, using the SIZE variable we defined globally.

    for (i = 0; i < SIZE; i++) {        //A for loop, one of three essential loops to know. This will be iterated on
        cout << "Enter a number: ";     //later on. All this one does is allow us to say "repeat this until a specific
        cin >> nums[i];                 //variable reaches a specific condition, and also perform an action on completion."
    }                                   //In this case, we're looping this function SIZE times.

    printSumAvg(nums);                  //This is a function call, passing the nums array in by value.
                                        //"By value" and "by reference" will be iterated upon later.

    return 0;                           //This is a standard for allowing us to say "This program has ended it's
                                        //lifecycle without any issues."
}

void printSumAvg(const double input[]) {            //The code for the function that we prototyped above main. You'll
    double sum = 0;                                 //probably notice that we passed a variable that is not constant, but
    int i;                                          //decided to declare that it's constant here. That's primarily because
    for (i = 0; i < SIZE; i++) {                    //we passed nums by value, and are thus unable to change the data. It's
        sum += input[i];                            //not necessarily required, but is a recommended action.
    }
    cout << "The sum of the numbers is: " << sum;   //The use of the exact pointy things is allow us to tack the value of
    cout << "\nThe average is: " << sum / SIZE;     //both sum and the result of dividing sum with SIZE (the average)
                                                    //rather easily. Also note how we added the \n at the start of the
                                                    //second cout. You could add endl at the end of the first one, though
                                                    //doing it like this also saves on the amount of characters I could
                                                    //type out, allowing me to optimize the filesize even by a little
                                                    //bit (These kinds of things will add up over time).
}